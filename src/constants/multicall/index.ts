import { ChainId } from '@koingfu.com/sdk-bch'
import MULTICALL_ABI from './abi.json'

const MULTICALL_NETWORKS: { [chainId in ChainId]: string } = {
  [ChainId.MAINNET]: '0x36BD320fAB5b9d2CFd81FBcf6046F81B4F71A0BC',
  [ChainId.AMBER]: '0xd337ef88a3aE59D1130bB31D365de407D8a85e6a'
}

export { MULTICALL_ABI, MULTICALL_NETWORKS }
